Custom Bootlogos
==========================
- Black Logos now avaible

- Modified Bootlogos For Ubuntu Touch devices

- Disclaimer
  ==========

  - Please note, I am not responsible for any device break/brick, this bootlogos are modifications use at your own risk.
    ====================================================================================================================

- If you like my work please feel free to donate me at:
  =====================================================
  - https://www.paypal.me/rubencarneiro
  - https://pt.liberapay.com/rubencarneiro/

Nexus 5
=======
**How to flash**:

- Reboot device to fastboot
- `fastboot flash imgdata imgdata.img`


Fairphone 2
===========
**How to flash**:

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash splash splash.img`


Meizu MX4
=========
**How to flash**:

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ E4.5
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ E5
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ M10HD
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


BQ M10FHD
=========
**How to flash**

- Reboot device to fastboot
- In a terminal and do:
- `fastboot flash logo logo.bin`


OnePlus One
=========
**How to flash**

- Reboot device to fastboot
- In a terminal do:
- `fastboot flash LOGO logo.bin`
